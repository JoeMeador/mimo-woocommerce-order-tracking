# MIMO Woocommerce Order Tracking

This plugin provides a way for woocommerce admins to attach tracking information to an order and upon updating the order the shipment and tracking info is added to the email sent to the customer.