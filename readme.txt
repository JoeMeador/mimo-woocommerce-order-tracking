=== MIMO Woocommerce Order Tracking ===
Contributors:      surakrai
Donate link:       https://www.paypal.me/surakraisam/5
Tags: woocommerce, delivery, order shipping, order shipping, track email
Requires at least: 4.0.1
Tested up to: 5.1
Stable tag: 1.0.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add shipping tracking information to your orders.

== Description ==
MIMO Woocommerce Order Tracking is a WordPress plugin that manage information for shipping your orders and that lets your customers be notified about their order shipping.

= Features =

* Can add service providers.
* Automatically change orders status to completed .
* Can add shipping information in your email completing.

How to Add a Provider
Go to WooCommerce -> Settings -> Order Tracking Tab.

== Installation ==

Manual Installation

1. Upload the entire `/mimo-woocommerce-order-tracking` directory to the `/wp-content/plugins/` directory.
2. Activate MIMO Woocommerce Order Tracking through the 'Plugins' menu in WordPress.

== Screenshots ==

1. Settings
2. Shipment Tracking
3. Tracking info on the frontend

== Changelog ==
= 1.0.2 =
* Fix bug adding provider
= 1.0.1 =
* Fix issue provider name
= 1.0.0 =
* Add the shipping information on the order tracking page on woocommerce
* Add option email sender shipping information of status
* compatibility with WooCommerce.
= 0.9.2 =
* Set today as the default date shipped
= 0.9.1 =
* First release