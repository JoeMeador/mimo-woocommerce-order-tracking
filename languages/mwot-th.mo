��          �   %   �      @      A     b     o     u     �     �     �  g   �                8     O     c     �     �     �     �     �     �     �     �     �     �       �    K   �  3   A  	   u  $        �  0   �  $   �  �     -   �  T        c  ?   �  9   #	  B   ]	  $   �	  0   �	     �	  $   	
  -   .
  -   \
     �
  !   �
  $   �
     �
        	                                   
                                                                                   Add a tracking number to the URL Add provider Close Date shipped Delete Email description Email setting MIMO Woocommerce Order Tracking is enabled but not effective. It requires WooCommerce in order to work. Order Tracking Order Tracking Settings Order tracking sended. Please enter a date Please enter a tracking number Please select a provider Provider Provider name Save Save and Send Shipment Tracking Shipping information Track Tracking URL Tracking number Update Project-Id-Version: MIMO Woocommerce Order Tracking
POT-Creation-Date: 2018-05-06 09:59+0700
PO-Revision-Date: 2018-05-06 10:02+0700
Last-Translator: 
Language-Team: Surakrai Nookong <surakraisam@gmail.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 เพิ่มหมายเลขเพัสดุในลิงค์ เพิ่มผู้ให้บริการ ปิด วันที่จัดส่ง ลบ คำอธิบายในอีเมล์ ตั้งค่าอีเมล MIMO Woocommerce Order Tracking เปิดใช้แล้วแต่ไม่สามรถทำงานได้เต็มประสิทธิภาพ ต้องลงปลั๊กอิน WooCommerce ติดตามการจัดส่ง ตั้งค่าการติดตามจัดส่งสินค้า บันทึกและส่ง ข้อมูลติดตามการจัดส่งเรียบร้อย กรุณากรอกวันที่จัดส่ง กรุณากรอกหมายเพัสดุ กรุณาเลือกผู้ให้บริการ ผู้ให้บริการ ชื่อผู้ให้บริการ บันทึก บันทึกและส่ง ติดตามการจัดส่ง ข้อมูลการจัดส่ง ติดตาม ลิงค์ติดตาม หมายเลขพัสดุ อัพเดท 